/* eslint @typescript-eslint/no-var-requires: 0 */

/**
 * This program builds rule.d.ts and rule.schema.json from the sample.rule.json
 * provided in this folder. Files are saved in the /src/ directory. Files in
 * this directory are not included in a build of the DevRM app.
 *
 */

const toJsonSchema = require('to-json-schema');
const sampleRule = require('./sample.rule.json');
const { compile } = require('json-schema-to-typescript');
const fs = require('fs');

console.log('Converting sample.rule.json to TypeScript types.');

const typePath = './src/rule.d.ts';
const schemaPath = './src/rule.schema.json';

// Delete current JSON schema and type files.
try {
  console.log('Deleting old files...');
  fs.unlinkSync(typePath, (err) => {
    console.error(err);
  });
  fs.unlinkSync(schemaPath, (err) => {
    console.error(err);
  });
} catch (error) {
  console.error('One of the files does not exist or could not be deleted.');
}

// Convert the sample.rule.json to a JSON schema.
console.log('Converting to JSON schema...');
const schema = toJsonSchema(sampleRule, { required: true, arrays: { mode: 'uniform' } });

console.log('Saving JSON schema...');
fs.writeFileSync(schemaPath, JSON.stringify(schema, null, 4));

console.log('Saving Types...');
compile(schema, 'rule', { format: true }).then((ts) => fs.writeFileSync(typePath, ts));

console.log('Done.');
