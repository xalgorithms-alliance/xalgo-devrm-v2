# Xalgorithms Developer Rule Maker v2

[![FOSSA Status](https://app.fossa.com/api/projects/git%2Bgitlab.com%2Fxalgorithms-alliance%2Fxalgo-devrm-v2.svg?type=shield)](https://app.fossa.com/projects/git%2Bgitlab.com%2Fxalgorithms-alliance%2Fxalgo-devrm-v2?ref=badge_shield)

Text editor to modify `.xa.json` files.

Currently in development and not ready for general use.

A [Figma UI mockup](https://www.figma.com/proto/v0P92KBcIJAniZMsPl6Y5k/IDE?page-id=2478%3A45269&node-id=2491%3A45542&viewport=238%2C191%2C0.06510163098573685&scaling=scale-down)
has been designed to inform functionality and basic layout choices.

## Download the App

Executables are built in Gitlab CI for the following platforms. Click to download the latest release:

- [Linux (Deb/RPM)](https://gitlab.com/xalgorithms-alliance/xalgo-devrm-v2/-/jobs/artifacts/master/download?job=build-linux)
- [MacOS](https://gitlab.com/xalgorithms-alliance/xalgo-devrm-v2/-/jobs/artifacts/master/download?job=build-osx)
- [Windows](https://gitlab.com/xalgorithms-alliance/xalgo-devrm-v2/-/jobs/artifacts/master/download?job=build-windows)

## Licensing

[![FOSSA Status](https://app.fossa.com/api/projects/git%2Bgitlab.com%2Fxalgorithms-alliance%2Fxalgo-devrm-v2.svg?type=large)](https://app.fossa.com/projects/git%2Bgitlab.com%2Fxalgorithms-alliance%2Fxalgo-devrm-v2?ref=badge_large)

## Development

Install node version manager, and use npm to install yarn globally.

### Re-Generating Rule Types/Schema

Run `yarn build-schema` after modifying the `sample.rule.json` in `/schema/`
