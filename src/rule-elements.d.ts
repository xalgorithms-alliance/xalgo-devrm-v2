/* eslint @typescript-eslint/no-empty-interface: 0 */
import { Rule } from './rule';

type ArrayElement<ArrayType extends readonly unknown[]> = ArrayType extends readonly (infer ElementType)[]
  ? ElementType
  : never;

type InputConditions = Rule['input_conditions'];
export type InputCondition = ArrayElement<NonNullable<InputConditions>>;

type OutputAssertions = Rule['output_assertions'];
export type OutputAssertion = ArrayElement<NonNullable<OutputAssertions>>;
