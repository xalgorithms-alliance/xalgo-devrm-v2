// From https://github.com/Xalgorithms/rm-functions/blob/master/src/utilities.js

export const alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
export const alpha_len = alphabet.length;

/**
 * Generates an input/output case ID from the previous int/char combination.
 * @param {string} prevValue
 */
export function generateCaseValueFromPrev(prevValue: string): string {
  if (typeof prevValue !== 'string') throw Error('Could not generate next case, value is not a string.');
  if (prevValue.length === 1) {
    const nextVal = alphabet.indexOf(prevValue) + 1;
    return generateCaseValue(nextVal);
  } else {
    const valueLen = prevValue.length;
    const number = parseInt(prevValue.substring(0, valueLen - 1));
    const letter = alphabet.indexOf(prevValue.substring(valueLen - 1, valueLen));
    const nextVal = number * alpha_len + letter + 1;
    return generateCaseValue(nextVal);
  }
}

/**
 * Generates an input/output case ID from its numerical position in the cases.
 * @param {int} number
 */
export function generateCaseValue(number: number): string {
  const pre = Math.floor(number / alpha_len);
  const cha = alphabet.charAt(number % alpha_len);
  if (pre === 0) {
    return cha.toUpperCase();
  } else {
    return `${pre}${cha}`;
  }
}
