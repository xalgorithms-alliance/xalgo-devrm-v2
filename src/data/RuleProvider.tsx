/* eslint @typescript-eslint/no-non-null-assertion: 0 */

import fs from 'fs';
import produce from 'immer';
import { Draft04 as JSL } from 'json-schema-library';
import React, { createContext, FC, ReactElement, useEffect, useState } from 'react';
import { Notification } from 'rsuite';
import { Rule } from '../rule';
import { InputCondition, OutputAssertion } from '../rule-elements';
import schema from '../rule.schema.json';
import { generateCaseValue } from './Utilities';

const jsl = new JSL(schema);

const inputConditionSchema = jsl.step('input_conditions', schema, { input_conditions: 'element' });
const inputConditionValidator = new JSL(inputConditionSchema);

const outputAssertionsSchema = jsl.step('input_conditions', schema, { input_conditions: 'element' });
const outputAssertionsValidator = new JSL(outputAssertionsSchema);

export type Sentence = InputCondition[] | OutputAssertion[] | undefined;

export type SetSentence = React.Dispatch<React.SetStateAction<Rule['input_conditions'] | Rule['output_assertions']>>;

export interface AppState {
  filePath: string;
  folderPath: string;
  files: string[];
  welcomeModalOpen: boolean;
  folderPickerModalOpen: boolean;
}

// Default app state
const defaultState: AppState = {
  filePath: '',
  folderPath: '',
  files: [],
  welcomeModalOpen: true,
  folderPickerModalOpen: false,
};

export interface Actions {
  ping: () => void;
  getRule: () => Rule;
  setRule: (validatedContent: Rule) => void;
  save: () => void;
  saveAs: (fullPath: string) => void;
  saveNew: (fullPath: string) => void;
  open: (fullPath: string) => void;
  newInputOutputCase: () => void;
  newInputCondition: () => void;
  newOutputAssertion: () => void;
  openFolder: (dirPath: string) => void;
  deleteCase: (index: number) => void;
  renameCase: (newName: string, index: number) => void;
  moveCase: (from: number, to: number) => void;
  moveSentence: (from: number, to: number, setSentence: SetSentence) => void;
}

export interface RuleContextProvider extends Rule {
  // Application Control Elements
  appState: AppState;
  actions: Actions;
  setAppState: React.Dispatch<React.SetStateAction<AppState>>;
  // Rule Properties
  setUUID: React.Dispatch<React.SetStateAction<Rule['uuid']>>;
  setRuleReserveNodes: React.Dispatch<React.SetStateAction<Rule['rulereserve_nodes']>>;
  setVersionStandardURL: React.Dispatch<React.SetStateAction<Rule['version_standard_url']>>;
  setXalgoSchemaVersion: React.Dispatch<React.SetStateAction<Rule['xalgo_schema_version']>>;
  // Rule Elements
  setMetadata: React.Dispatch<React.SetStateAction<Rule['metadata']>>;
  setLinkedRulesOrLookups: React.Dispatch<React.SetStateAction<Rule['linked_rules_or_lookups']>>;
  setContextInEffect: React.Dispatch<React.SetStateAction<Rule['context_ineffect']>>;
  setCategoryApplicable: React.Dispatch<React.SetStateAction<Rule['category_applicable']>>;
  setInputConditions: React.Dispatch<React.SetStateAction<Rule['input_conditions']>>;
  setOutputAssertions: React.Dispatch<React.SetStateAction<Rule['output_assertions']>>;
  setOutputWeight: React.Dispatch<React.SetStateAction<Rule['output_weight']>>;
  setOutputCharacteristics: React.Dispatch<React.SetStateAction<Rule['output_characteristics']>>;
}

export const RuleContext = createContext<RuleContextProvider>({} as RuleContextProvider);

const RuleProvider: FC<{ children: ReactElement | null }> = ({ children = null }) => {
  // Spin up an empty rule object to instantiate state.
  const r: Rule = jsl.getTemplate({}) as Rule;

  /// Rule State Hooks - Every rule section has independent state to improve performance.

  // Properties
  const [uuid, setUUID] = useState<Rule['uuid']>(r.uuid);
  const [rulereserve_nodes, setRuleReserveNodes] = useState<Rule['rulereserve_nodes']>(r.rulereserve_nodes);
  const [version_standard_url, setVersionStandardURL] = useState<Rule['version_standard_url']>(r.version_standard_url);
  const [xalgo_schema_version, setXalgoSchemaVersion] = useState<Rule['xalgo_schema_version']>(r.xalgo_schema_version);

  // Elements
  const [metadata, setMetadata] = useState<Rule['metadata']>(r.metadata);
  const [linked_rules_or_lookups, setLinkedRulesOrLookups] = useState<Rule['linked_rules_or_lookups']>(
    r.linked_rules_or_lookups
  );
  const [context_ineffect, setContextInEffect] = useState<Rule['context_ineffect']>(r.context_ineffect);
  const [category_applicable, setCategoryApplicable] = useState<Rule['category_applicable']>(r.category_applicable);
  const [input_conditions, setInputConditions] = useState<Rule['input_conditions']>(r.input_conditions);
  const [output_assertions, setOutputAssertions] = useState<Rule['output_assertions']>(r.output_assertions);
  const [output_weight, setOutputWeight] = useState<Rule['output_weight']>(r.output_weight);
  const [output_characteristics, setOutputCharacteristics] = useState<Rule['output_characteristics']>(
    r.output_characteristics
  );

  // App State Hooks
  const [appState, setAppState] = useState<AppState>(defaultState);

  const openFolder = (path = '') => {
    if (!path) path = appState.filePath;
    if (!path) throw new Error('No folder specified!');
    fs.readdir(path, (err, files) => {
      if (err) {
        console.error('Could not read the files.');
        return;
      }
      console.log(`Opening folder at ${path}`);
      console.log(files);
      setAppState((prev) =>
        produce(prev, (draft) => {
          draft.folderPath = path;
          draft.files = files;
          localStorage.setItem('open-folder', path);
        })
      );
    });
  };

  /**
   * On app startup, re-opens previously opened folder.
   */
  useEffect(() => {
    if (appState.folderPath) return;
    console.log('Checking for previously opened folder on startup...');
    const folderPath = localStorage.getItem('open-folder');
    if (folderPath) openFolder(folderPath);
  }, [openFolder, setAppState, appState]);

  const ping = (): void => {
    console.log('pong!');
  };

  /**
   * @returns the full rule with all sections included.
   */
  const getRule = (): Rule => ({
    // Properties
    uuid: uuid,
    rulereserve_nodes: rulereserve_nodes,
    version_standard_url: version_standard_url,
    xalgo_schema_version: xalgo_schema_version,
    // Elements
    metadata: metadata,
    linked_rules_or_lookups: linked_rules_or_lookups,
    context_ineffect: context_ineffect,
    category_applicable: category_applicable,
    input_conditions: input_conditions,
    output_assertions: output_assertions,
    output_weight: output_weight,
    output_characteristics: output_characteristics,
  });

  const setRule = (validatedContent: Rule) => {
    const v: Rule = jsl.getTemplate(validatedContent) as Rule;
    // Properties
    setUUID(v.uuid);
    setRuleReserveNodes(v.rulereserve_nodes);
    setVersionStandardURL(v.version_standard_url);
    setXalgoSchemaVersion(v.xalgo_schema_version);
    // Elements
    setMetadata(v.metadata);
    setLinkedRulesOrLookups(v.linked_rules_or_lookups);
    setContextInEffect(v.context_ineffect);
    setCategoryApplicable(v.category_applicable);
    setInputConditions(v.input_conditions);
    setOutputAssertions(v.output_assertions);
    setOutputWeight(v.output_weight);
    setOutputCharacteristics(v.output_characteristics);
  };

  /**
   *
   * @param fullPath full path to *.rule.json file to open.
   */
  const open = (fullPath: string) => {
    try {
      // Check if file exists, is a file (and not a directory,) and is named correctly.
      const fileExists = fs.existsSync(fullPath);
      if (!fileExists) throw new Error('File does not exist! Canot open.');
      const stat = fs.lstatSync(fullPath);
      if (!stat.isFile()) throw new Error('Element is not a file object.');
      if (!fullPath.endsWith('.rule.json')) throw new Error('File must end with .rule.json');

      // Read file and validate.
      const file = fs.readFileSync(fullPath, 'utf-8');
      const json = JSON.parse(file);
      const errors: { message: string }[] = jsl.validate(json);
      if (errors.length === 0) {
        console.log('Loaded rule from filesystem.');
        console.log(JSON.stringify(json, null, 2));
        setAppState((prev) =>
          produce(prev, (draft) => {
            draft.filePath = fullPath;
          })
        );
        setRule(json as Rule);
        return;
      } else {
        console.error(errors);
        throw new Error(errors[0].message);
      }
    } catch (error) {
      console.error(error);
      Notification['error']({
        title: 'Failed to Open File',
        description: error.toString(),
      });
    }
  };

  const save = () => {
    fs.writeFileSync(appState.filePath, JSON.stringify(getRule(), null, 4));
  };

  const saveAs = (fullPath: string) => saveAsSubfunction(fullPath, false);
  const saveNew = (fullPath: string) => saveAsSubfunction(fullPath, true);

  const saveAsSubfunction = (fullPath: string, blank: boolean) => {
    const fileExists = fs.existsSync(fullPath);
    if (fileExists) throw new Error('File already exists!');
    fs.writeFileSync(fullPath, JSON.stringify(blank ? jsl.getTemplate({}) : getRule(), null, 4));
    setAppState((prev) =>
      produce(prev, (draft) => {
        draft.filePath = fullPath;
      })
    );
  };

  const newInputCondition = () => {
    setInputConditions((prev) => {
      if (!prev) prev = [] as InputCondition[];
      return produce(prev, (draft) => {
        // Create blank input condition from schema.
        const blankIC = inputConditionValidator.getTemplate([{}])[0] as InputCondition;
        blankIC.logic_values = [];

        // Populate with cases from the first of its kind, or the opposite type (OA/IC)
        if ((!draft || draft.length === 0) && (!output_assertions || output_assertions.length === 0)) {
          blankIC.logic_values.push({
            scenario: 'A',
            value: '00',
          });
        } else {
          // Copy the logic values from the first input condition (or first output assertion).
          const sampleIC: InputCondition | undefined =
            (draft && draft[0]) || (output_assertions && output_assertions[0]);
          // If the sample selected does not have logic values, freak out.
          if (sampleIC === undefined) throw new Error('Error occurred while tyring to add an input condition.');
          if (!sampleIC.logic_values) throw new Error('First Input Condition has no logic values.');
          // Copy the scenario labels from the target sentence.
          for (let i = 0; i < sampleIC.logic_values.length; i++) {
            blankIC.logic_values.push({
              scenario: sampleIC.logic_values[i].scenario,
              value: '00',
            });
          }
        }

        // Finally, push the new condition to the draft.
        draft.push(blankIC);
      });
    });
  };

  const newOutputAssertion = () => {
    setOutputAssertions((prev) => {
      if (!prev) prev = [] as OutputAssertion[];
      return produce(prev, (draft) => {
        // Create blank input condition from schema.
        const blankOA = outputAssertionsValidator.getTemplate([{}])[0] as OutputAssertion;
        blankOA.logic_values = [];

        // Populate with cases from the first of its kind, or the opposite type (OA/IC)
        if ((!draft || draft.length === 0) && (!input_conditions || input_conditions.length === 0)) {
          blankOA.logic_values.push({
            scenario: 'A',
            value: '00',
          });
        } else {
          // Copy the logic values from the first input condition (or first output assertion).
          const sampleOA: OutputAssertion | undefined =
            (draft && draft[0]) || (input_conditions && input_conditions[0]);
          // If the sample selected does not have logic values, freak out.
          if (sampleOA === undefined) throw new Error('Error occurred while tyring to add an input condition.');
          if (!sampleOA.logic_values) throw new Error('First Input Condition has no logic values.');
          // Copy the scenario labels from the target sentence.
          for (let i = 0; i < sampleOA.logic_values.length; i++) {
            blankOA.logic_values.push({
              scenario: sampleOA.logic_values[i].scenario,
              value: '00',
            });
          }
        }

        // Finally, push the new condition to the draft.
        draft.push(blankOA);
      });
    });
  };

  const newInputOutputCase = () => {
    try {
      // Determine a letter for the new case.
      let scenario: string | null = null;
      let scenarioNumber = 0;
      const first_ic: InputCondition | undefined = input_conditions && input_conditions[0];
      const first_oa: OutputAssertion | undefined = output_assertions && output_assertions[0];
      while (scenario === null) {
        const potentialScenario = generateCaseValue(scenarioNumber++);
        if (first_ic && first_ic.logic_values && first_ic.logic_values.some((v) => v.scenario === potentialScenario))
          continue;
        if (first_oa && first_oa.logic_values && first_oa.logic_values.some((v) => v.scenario === potentialScenario))
          continue;

        // If the scenario is not found in the input or output assertions, it can be used.
        scenario = potentialScenario;
      }

      // Add the new case to all sentences.
      const addCase = (prev: Sentence | undefined): Sentence =>
        produce(prev, (draft) => {
          if (draft === undefined || draft.length === 0) return [];
          for (let i = 0; i < draft.length; i++) {
            if (draft[i].logic_values === undefined) draft[i].logic_values = [];
            draft[i].logic_values?.push({
              scenario: scenario || '',
              value: '00',
            });
          }
        });

      console.log('Adding one new output assertion case.');
      setOutputAssertions((prev) => addCase(prev));
      console.log('Adding one new input condition case.');
      setInputConditions((prev) => addCase(prev));
    } catch (error) {
      console.error(error);
      Notification['error']({
        title: 'Error',
        description: error,
      });
    }
  };

  const deleteCase = (index: number) => {
    setInputConditions((prev) =>
      produce(prev, (draft) => {
        if (!draft) throw new Error('Case does not exist.');
        for (let i = 0; i < draft.length; i++) {
          draft![i].logic_values!.splice(index, 1);
        }
      })
    );
    setOutputAssertions((prev) =>
      produce(prev, (draft) => {
        if (!draft) throw new Error('Case does not exist.');
        for (let i = 0; i < draft.length; i++) {
          draft![i].logic_values!.splice(index, 1);
        }
      })
    );
  };

  const renameCase = (newName: string, index: number) => {
    setInputConditions((prev) =>
      produce(prev, (draft) => {
        if (!draft) throw new Error('Case does not exist.');
        for (let i = 0; i < draft.length; i++) {
          draft![i].logic_values![index].scenario = newName.toString();
        }
      })
    );
    setOutputAssertions((prev) =>
      produce(prev, (draft) => {
        if (!draft) throw new Error('Case does not exist.');
        for (let i = 0; i < draft.length; i++) {
          draft![i].logic_values![index].scenario = newName.toString();
        }
      })
    );
  };

  const moveCase = (from: number, to: number) => {
    setInputConditions((prev) =>
      produce(prev, (draft) => {
        if (!draft) throw new Error('Case does not exist.');
        for (let i = 0; i < draft.length; i++) {
          draft![i].logic_values!.splice(to, 0, draft![i].logic_values!.splice(from, 1)[0]);
        }
      })
    );
    setOutputAssertions((prev) =>
      produce(prev, (draft) => {
        if (!draft) throw new Error('Case does not exist.');
        for (let i = 0; i < draft.length; i++) {
          draft![i].logic_values!.splice(to, 0, draft![i].logic_values!.splice(from, 1)[0]);
        }
      })
    );
  };

  const moveSentence = (from: number, to: number, setSentence: SetSentence) => {
    setSentence((prev) =>
      produce(prev, (draft) => {
        if (!draft) throw new Error('Sentence does not exist.');
        draft.splice(to, 0, draft.splice(from, 1)[0]);
      })
    );
  };

  const providerValue: RuleContextProvider = {
    // Properties
    uuid: uuid,
    setUUID: setUUID,
    rulereserve_nodes: rulereserve_nodes,
    setRuleReserveNodes: setRuleReserveNodes,
    version_standard_url: version_standard_url,
    setVersionStandardURL: setVersionStandardURL,
    xalgo_schema_version: xalgo_schema_version,
    setXalgoSchemaVersion: setXalgoSchemaVersion,

    // Elements
    metadata: metadata,
    setMetadata: setMetadata,
    linked_rules_or_lookups: linked_rules_or_lookups,
    setLinkedRulesOrLookups: setLinkedRulesOrLookups,
    context_ineffect: context_ineffect,
    setContextInEffect: setContextInEffect,
    category_applicable: category_applicable,
    setCategoryApplicable: setCategoryApplicable,
    input_conditions: input_conditions,
    setInputConditions: setInputConditions,
    output_assertions: output_assertions,
    setOutputAssertions: setOutputAssertions,
    output_weight: output_weight,
    setOutputWeight: setOutputWeight,
    output_characteristics: output_characteristics,
    setOutputCharacteristics: setOutputCharacteristics,

    // App State
    appState: appState,
    setAppState: setAppState,
    actions: {
      ping: ping,
      getRule: getRule,
      setRule: setRule,
      save: save,
      saveAs: saveAs,
      saveNew: saveNew,
      open: open,
      newInputOutputCase: newInputOutputCase,
      newInputCondition: newInputCondition,
      newOutputAssertion: newOutputAssertion,
      openFolder: openFolder,
      deleteCase: deleteCase,
      renameCase: renameCase,
      moveCase: moveCase,
      moveSentence: moveSentence,
    },
  };

  return (
    <div>
      <RuleContext.Provider value={providerValue}>
        <div>{children}</div>
      </RuleContext.Provider>
    </div>
  );
};

export default RuleProvider;
