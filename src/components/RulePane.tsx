import React, { FC } from 'react';
import { MemoryRouter, Route, Switch, useHistory, useLocation } from 'react-router-dom';
import { Nav } from 'rsuite';
import { useXaTranslation } from '../i18n';
import Metadata from '../sections/Metadata';
import RuleJSON from '../sections/RuleJSON';
import SentenceTable from '../sections/SentenceTable';
import RuleEditor from './RuleEditor';

const PaneNavigation: FC = () => {
  const location = useLocation();
  const history = useHistory();

  return (
    <Nav
      activeKey={location.pathname}
      onSelect={(eventKey) => {
        history.push(eventKey);
      }}
      appearance="tabs"
    >
      <Nav.Item eventKey={'/'}>Rule</Nav.Item>
      <Nav.Item eventKey={'/json'}>JSON</Nav.Item>
      <Nav.Item eventKey={'/settings'}>Settings</Nav.Item>
    </Nav>
  );
};

/**
 *
 * @param last If the pane is the furthest to the right.
 */
const RulePane: FC<{ last: boolean }> = ({ last }) => {
  const { t } = useXaTranslation();

  return (
    <div>
      {/* <MemoryRouter initialEntries={[{ pathname: last ? '/json' : '/' }]}> */}
      <MemoryRouter initialEntries={['/']}>
        <PaneNavigation />
        <Switch>
          <Route path="/settings">
            <div style={{ overflowY: 'scroll' }}>
              <br />
              <h2>Settings</h2>
            </div>
          </Route>
          <Route path="/json">
            <div style={{ overflowY: 'scroll' }}>
              <br />
              <h2>JSON</h2>
              <RuleJSON />
            </div>
          </Route>
          <Route path="/">
            <div style={{ overflowY: 'scroll' }}>
              <br />
              <h2>Rule</h2>
              <br />
              <RuleEditor />
            </div>
          </Route>
        </Switch>
      </MemoryRouter>
    </div>
  );
};

export default RulePane;
