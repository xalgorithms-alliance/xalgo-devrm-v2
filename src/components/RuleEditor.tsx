import React, { FC } from 'react';
import AboutRuleForm from '../components/AboutRuleForm';
import SentenceTable from '../sections/SentenceTable';

/**
 * Displays all rule sections.
 * TODO: Add nav and allow user to toggle between continuous/sectional views.
 */
const RuleEditor: FC = () => {
  return (
    <>
      <AboutRuleForm />
      <SentenceTable />
    </>
  );
};

export default RuleEditor;
