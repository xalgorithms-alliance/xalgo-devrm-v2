import React, { FC, useState } from 'react';
import { FlexboxGrid, Nav } from 'rsuite';
import { useXaTranslation } from '../i18n';
import RulePane from './RulePane';

const RulePaneContainer: FC = () => {
  const { t } = useXaTranslation();
  const [panes, setPanes] = useState<number>(1);

  return (
    <FlexboxGrid
      align="top"
      justify="start"
      style={{ flexWrap: 'nowrap', maxWidth: '100%', maxHeight: 'calc(100vh - 70px)' }}
    >
      {Array(panes)
        .fill(1)
        .map((value, index) => (
          <FlexboxGrid.Item key={index} order={index} style={{ flexGrow: 1, flexShrink: 1 }}>
            <RulePane last={index === panes - 1} />
          </FlexboxGrid.Item>
        ))}
      <FlexboxGrid.Item order={panes + 1} style={{ flexGrow: 1, flexShrink: 1 }}>
        <Nav appearance="tabs">
          <Nav.Item
            onSelect={() => {
              setPanes((prev) => {
                if (prev > 1) return prev - 1;
                return prev;
              });
            }}
          >
            -
          </Nav.Item>
          <Nav.Item
            onSelect={() => {
              setPanes((prev) => prev + 1);
            }}
          >
            +
          </Nav.Item>
        </Nav>
      </FlexboxGrid.Item>
    </FlexboxGrid>
  );
};

export default RulePaneContainer;
