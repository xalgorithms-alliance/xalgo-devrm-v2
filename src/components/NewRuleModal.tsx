import fs from 'fs';
import path from 'path';
import React, { CSSProperties, FC, useContext, useState } from 'react';
import { Button, FlexboxGrid, Icon, IconButton, Input, InputGroup, Modal, Notification } from 'rsuite';
import sanitize from 'sanitize-filename';
import { RuleContext } from '../data/RuleProvider';
import { useXaTranslation } from '../i18n';
import FolderPicker from './FolderPicker';

const NewRuleModal: FC<{ onClick?: () => void; style?: CSSProperties }> = ({ onClick, style }) => {
  const { t } = useXaTranslation();
  const [open, setOpen] = useState(false);
  const { appState, actions } = useContext(RuleContext);
  const [fileName, setFileName] = useState<string>('');

  const errorNotification = (error: string) => {
    console.error(error);
    Notification['error']({
      title: t('errors.couldNotCreate.title'),
      description: t('errors.couldNotCreate.description') + ` Err: ${error}`,
      duration: 5000,
    });
  };

  const createRule = () => {
    const safeName = `${sanitize(fileName)}.rule.json`;
    const newFilePath = path.join(appState.folderPath, safeName);
    // First, check if the file exists on the filesystem.
    const fileExists = fs.existsSync(newFilePath);
    if (fileExists) {
      errorNotification('File already exists, cannot create.');
      return; // Return early and do not write.
    }
    // Otherwise, write the new file and set the app state.
    try {
      actions.saveNew(newFilePath);
      actions.open(newFilePath);
      setOpen(false);
    } catch (error) {
      errorNotification(error);
    }
  };

  return (
    <>
      <Modal show={open} onHide={() => setOpen(false)}>
        <Modal.Header>
          <Modal.Title>{t('newRuleModal.title')}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p style={{ marginTop: 4, marginBottom: 10 }}>
            <b>{'Folder: '}</b>
            {appState.folderPath}
          </p>
          {appState.folderPath ? (
            <>
              <p>{t('newRuleModal.newRulePrompt')}</p>
              <InputGroup>
                <Input value={fileName} onChange={(value: string) => setFileName(value)} />
                <InputGroup.Addon>{'.rule.json'}</InputGroup.Addon>
              </InputGroup>
              <FlexboxGrid justify="end" style={{ marginTop: 20 }}>
                <FlexboxGrid.Item>
                  <IconButton
                    appearance="primary"
                    disabled={!fileName}
                    icon={<Icon icon="check" />}
                    onClick={createRule}
                  >
                    {t('newRuleModal.create')}
                  </IconButton>
                </FlexboxGrid.Item>
              </FlexboxGrid>
            </>
          ) : (
            <>
              <FolderPicker />
            </>
          )}
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={() => setOpen(false)}>{t('general.close')}</Button>
        </Modal.Footer>
      </Modal>
      <IconButton
        icon={<Icon icon="plus" />}
        style={style}
        onClick={() => {
          setOpen(true);
          if (onClick) onClick();
        }}
      >
        {t('newRuleModal.buttonText')}
      </IconButton>
    </>
  );
};

export default NewRuleModal;
