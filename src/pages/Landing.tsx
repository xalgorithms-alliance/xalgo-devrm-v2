import React, { FC, useContext } from 'react';
import LanguageSwitcher from '../components/LanguageSwitcher';
import NewRuleModal from '../components/NewRuleModal';
import RulePicker from '../components/RulePicker';
import SaveRuleButton from '../components/SaveRuleButton';
import { RuleContext } from '../data/RuleProvider';
import { useXaTranslation } from '../i18n';
import Metadata from '../sections/Metadata';
import RuleJSON from '../sections/RuleJSON';
import SentenceTable from '../sections/SentenceTable';
import appInfo from '../../package.json';
import RulePaneContainer from '../components/RulePaneContainer';

const Landing: FC = () => {
  const { t } = useXaTranslation();
  const { appState } = useContext(RuleContext);
  return (
    <div style={{ padding: 20 }}>
      <div style={{ marginBottom: 20 }}>
        <h2>{t('appTitle')}</h2>
        <p>
          {t('appSubtitle')} {appInfo.version}
        </p>
      </div>
      <div
        style={{
          marginBottom: 30,
          position: 'sticky',
          top: 0,
          background: 'white',
          padding: '1rem',
          zIndex: 1000,
          height: '70px',
        }}
      >
        <LanguageSwitcher />
        <RulePicker style={{ marginLeft: 20, marginRight: 10 }} />
        <NewRuleModal style={{ marginRight: 20 }} />
        <SaveRuleButton style={{ marginRight: 10 }} />
      </div>
      <div style={{ marginTop: 4, marginBottom: 10 }}>
        <p>
          <b>{'Folder: '}</b> <code>{appState.folderPath}</code>
        </p>
        <p>
          <b>{'Editing: '}</b>
          <code>{appState.filePath}</code>
        </p>
      </div>
      {appState.filePath ? (
        <div style={{ marginTop: 50 }}>
          <RulePaneContainer />
        </div>
      ) : (
        <div style={{ marginTop: 50 }}>
          <p>{t('landing.openRule')}</p>
        </div>
      )}
    </div>
  );
};

export default Landing;
