/* eslint @typescript-eslint/no-var-requires: 0 */

const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');

module.exports = [
  new ForkTsCheckerWebpackPlugin({
    async: false,
  }),
];
